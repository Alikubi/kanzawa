# !/usr/bin/env python3

from bs4 import BeautifulSoup
import urllib.request,urllib.parse,json,sys

SEARCH_URL = 'http://www.gamespot.com/search/?q='
BASE_URL="http://www.gamespot.com"
USERAGENT="Mozilla/5.0"

# スープ作り担当関数
def making_soup(url):
    req = urllib.request.Request(url)
    req.add_header('User-agent',USERAGENT)
    res = urllib.request.urlopen(req)
    html = res.read()
    soup = BeautifulSoup(html,'html.parser')
    return soup

# 検索まわりを適当関数
def search(keywords):
    quoted = urllib.parse.quote(keywords)
    html = making_soup(SEARCH_URL + quoted)
    return html

# 検索結果のリンク先に次々に飛んでいく関数
def access(url):
    # リンク先が含まれる記述の抽出
    for a in url.findAll('div', attrs={'class':'media-figure media-figure--search'}):
        # 強引に
        for b in a.findAll('a'):
            # リンク先のURLの一部 /yakuza/
            word_url = (dict(b.attrs)['href'])
            # 合体 http://www.gamespot.com + /yakuza/
            url = BASE_URL + word_url
            print ("URL: {}".format(url))
            # レビューのページまで飛ばすコードは無駄だと思ったので強引に指定
            # http://www.gamespot.com/yakuza/reviews/
            html = making_soup(url + "/reviews/")
            print ("Title: {}".format(html.title.text))
            info = extract_info(html)
            print (json.dumps(info,indent=4))
            with open("{}.json".format(html.title.text) , "a") as fh:
                json.dump(info,fh,indent=4)


# レーティングとユーザIDを抽出する関数
def extract_info(url):
    info_rating = []
    info_user = []  
    # レーティングが記述されている箇所の抽出
    for rating in url.findAll('div', attrs={'class':'media-well--review-user'}):
        # リストに追加
        info_rating.append(rating.text)
    # ユーザIDが記述されている箇所の抽出
    for a in url.findAll('div', attrs={'class':'userReview-list__byline'}):
        # 強引に
        for user in a.findAll('a'):
            # リストに追加
            info_user.append(user.text)
    # レーティングとユーザIDをペアにして辞書へ変換
    info_dict = {k:v for(k,v) in zip(info_user,info_rating)}
    return info_dict

if __name__ == "__main__":
    # 引数が足りてない場合は警告
    if len(sys.argv) <= 1:
        print("Usage: {} [keyword] ...".format(sys.argv[0]))
        exit(1)
    keywords = ' '.join(sys.argv[1: ])
    html = search(keywords)
    access(html)

